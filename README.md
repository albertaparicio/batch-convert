# Batch Convert

This Bash script converts all audio files of selected extension (default: mp3)
into the desired codec and bitrate (default AAC 128k).

It uses GNU Parallel to run several instances of ffmpeg at once, taking full
advantage of computers with multiple processors.

This script requires GNU Parallel and FFmpeg (v4 or later).

Its command-line options are listed in the help message (-h option)