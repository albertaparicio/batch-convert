#!/bin/bash -
#==============================================================================
#
#          FILE: batch_convert
#
#         USAGE:./batch_convert.sh [-h] [-v] [-n n_jobs] [-i in_ext]
#                                  [-o out_ext] [-b out_bitrate] [-d dir]
#                                  [-s size] [-c codec]
#
#   DESCRIPTION: Get files of desired extension in input directory (and its
#                subdirectories) and convert them to the desired output codec
#
#       OPTIONS: ---
#  REQUIREMENTS: GNU parallel, ffmpeg
#          BUGS: ---
#         NOTES:
#        AUTHOR: Albert Aparicio Isarn
#  ORGANIZATION:
#       CREATED: 09/03/2016 13:12
#      REVISION: 19/01/2019 20:04
#==============================================================================

# set -o nounset                              # Treat unset variables as an error

VERSION='2.0'
COPYRIGHT="batch_convert (Batch Convert) v$VERSION ©Copyright (C) 2016-2019 Albert Aparicio"
LICENSE="This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Source code available on https://gitlab.com/albertaparicio/batch-convert"

get_help(){
    echo "$COPYRIGHT

$LICENSE
--------------------------------------------------------------------------------

Convert audio files in parallel to the desired codec, bitrate and extension.
This program uses ffmpeg to convert the files.
The available arguments are optional.
Default values are listed in the description.

Syntax: $(basename "$0") [-h] [-v] [-n n_jobs] [-i in_ext] [-o out_ext]
                         [-b out_bitrate] [-d dir] [-s size] [-c codec]

Options:

    -d, --directory         Directory to search files in (includes subdirs)
                                (Default: current dir)
    -n, --num-jobs          Number of files to convert in parallel
                                (Default: number of cpu cores)
    -i, --input-extension   Extension of the input files to convert
                                (Default: mp3)
    -c, --output-codec      Codec of the output files
                                (Default: aac)
    -b, --output-bitrate    Bitrate of the output files in kbps
                                (Default: 128)
    -o, --output-extension  Extension of the output files
                                (Default: m4a)
    -s, --cover-size        Size in pixels of the cover art in the output files
                                (Default: 1024)

    -h, --help              Display this help message and exit
    -v, --version           Display the version number and exit
"

}

print_version(){
    echo "$VERSION"
}

usage(){
    echo "$(basename "$0") [-h] [-v] [-n n_jobs] [-i in_ext] [-o out_ext]
                 [-b out_bitrate] [-d dir] [-s size] [-c codec]"
}

# Conversion function.
convert_file()
{
    FILE="$1"
    OUT_BIT="$2"
    OUT_CODEC="$3"
    OUT_EXT="$4"
    COVER_SIZE="$5"

    ffmpeg -y -v 0 -i "$FILE" -threads 1 \
        -vcodec mjpeg -vsync 2 -vf scale="$COVER_SIZE":-1 \
        -acodec "$OUT_CODEC" -ab "$OUT_BIT"k -ac 2 -ar 44100 \
        "${FILE%.*}"."$OUT_EXT"
}
export -f convert_file



# Assign default values to variables
COVER_SIZE=1024
DIR='.'
IN_EXT='mp3'
OUT_BIT=128
OUT_CODEC='aac'
OUT_EXT='m4a'
NUM=$(nproc --all)

# Parse console arguments
if [ $# -ge 1 ]
then
    while [ "$1" != "" ]; do
        case $1 in
            -d | --directory )          shift
                                        DIR="$1"
                                        ;;
            -n | --num-jobs )           shift
                                        NUM="$1"
                                        ;;
            -i | --input-extension )    shift
                                        IN_EXT="$1"
                                        ;;
            -b | --output-bitrate )     shift
                                        OUT_BIT="$1"
                                        ;;
            -c | --output-codec)        shift
                                        OUT_CODEC="$1"
                                        ;;
            -o | --output-extension )   shift
                                        OUT_EXT="$1"
                                        ;;
            -s | --cover-size )    	    shift
                                        COVER_SIZE="$1"
                                        ;;
            -h | --help )               get_help
                                        exit 0
                                        ;;
            -v | --version )            print_version
                                        exit 0
                                        ;;
            * )                         usage
                                        exit 1
        esac
        shift
    done
fi

# Display license notice
echo "$COPYRIGHT

$LICENSE
"

# Run 'convert_file' function on each file in the selected directory and subdirectories
parallel -j $NUM --bar convert_file {1} {2} {3} {4} {5} ::: "$(find "$(readlink -f "$DIR")" -name "*.$IN_EXT")" ::: "$OUT_BIT" ::: "$OUT_CODEC" ::: "$OUT_EXT" ::: "$COVER_SIZE"

echo 'Finished!'

notify-send -a 'Batch Convert' -i musique 'Batch Convert: Finished converting'
